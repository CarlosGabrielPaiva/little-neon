package com.paivadeveloper.littleneon.view.paymenthistory

import android.app.Dialog
import android.os.Bundle
import android.view.MenuItem
import android.view.View
import android.view.Window
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.LinearLayoutManager
import com.google.android.material.snackbar.Snackbar
import com.paivadeveloper.littleneon.R
import com.paivadeveloper.littleneon.extensions.hasInternetConnection
import com.paivadeveloper.littleneon.service.repository.Repository
import com.paivadeveloper.littleneon.view.sendmoney.SendMoneyActivity
import com.paivadeveloper.littleneon.viewmodel.ViewModelFactory
import kotlinx.android.synthetic.main.activity_payment_history.*
import kotlinx.android.synthetic.main.content_payment_history.*
import kotlinx.android.synthetic.main.dialog_error.view.*

class PaymentHistoryActivity : AppCompatActivity() {

    private val paymentHistoryViewModel by lazy {
        ViewModelProviders.of(this, ViewModelFactory(this))
            .get(PaymentHistoryViewModel::class.java)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_payment_history)
        setSupportActionBar(toolbar)

        setUpObservers()
        configToolbar()
        configRecyclerView()

      paymentHistoryViewModel.getPaymentHistory()
    }

    private fun hideLoading() {
        lottieError.visibility = View.GONE
        recyclerViewHistoryPayment.visibility = View.VISIBLE
    }

    private fun showLoading() {
        lottieError.visibility = View.VISIBLE
        recyclerViewHistoryPayment.visibility = View.GONE
    }

    private fun setUpObservers() {
        paymentHistoryViewModel.loading.observe(this, Observer { isVisibleLoading ->
            when (isVisibleLoading) {
                true -> showLoading()
                false -> hideLoading()
            }
        })
        paymentHistoryViewModel.contactsList.observe(this, Observer { historyPaymentList ->
            recyclerViewHistoryPayment.adapter = PaymentHistoryListAdapter(historyPaymentList)
        })
        paymentHistoryViewModel.responseError.observe(this, Observer {errorType ->
            when(errorType){
                Repository.ErrorType.API_ERROR ->
                    showError(ERROR_API)

                Repository.ErrorType.CONNECTION_ERROR ->
                    showError(ERROR_INTERNET)
            }
        })
    }

    private fun showError(errorMessage: String) {
        val dialog = Dialog(this, R.style.NeonDialog)
        val view = layoutInflater.inflate(R.layout.dialog_error, null)


        view.textViewDialogErrorSubtitle.text = errorMessage
        view.buttonTryConection.text = SendMoneyActivity.ERROR_OK
        view.buttonTryConection.setOnClickListener {
            if (view.context.hasInternetConnection()){
                dialog.hide()
            }else{
                Snackbar.make(view, R.string.error_connection, Snackbar.LENGTH_LONG).show()
            }
        }
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
        dialog.setCancelable(true)
        dialog.setContentView(view)
        dialog.show()
    }

    private fun configToolbar() {
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        supportActionBar?.setDisplayShowHomeEnabled(true)
        val backArrow = resources.getDrawable(R.drawable.ic_left_arrow)
        supportActionBar?.setHomeAsUpIndicator(backArrow)
    }

    private fun configRecyclerView() {
        val layoutManager = LinearLayoutManager(this)
        recyclerViewHistoryPayment.layoutManager = layoutManager
        recyclerViewHistoryPayment.adapter = PaymentHistoryListAdapter(emptyList())
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        if (item.itemId === android.R.id.home) {
            finish()
        }

        return super.onOptionsItemSelected(item)
    }


    companion object{
        const val ERROR_INTERNET = "Nossos servidores estão em manutenção ;("
        const val ERROR_API = "Nossos servidores estão em manutenção ;("
    }
}
