package com.paivadeveloper.littleneon.view.sendmoney

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.paivadeveloper.littleneon.model.Contacts
import com.paivadeveloper.littleneon.model.SendMoney
import com.paivadeveloper.littleneon.service.repository.ServiceListener
import com.paivadeveloper.littleneon.service.repository.Repository

class SendMoneyViewModel(private val repository: Repository) : ViewModel(),
    ServiceListener {

    val contactsList = MutableLiveData<List<Contacts>>()
    val responseError = MutableLiveData<Repository.ErrorType>()
    val loading = MutableLiveData<Boolean>()
    val successSendMoney = MutableLiveData<SendMoney>()

    fun getContacts() {
        loading.value = true
        repository.getContacts(this)
    }

    fun sendMoney(contact: Contacts, amount: String) {
        repository.sendMoney(this, contact, amount)
    }

    override fun onSuccess(response: Any) {
        contactsList.value = response as MutableList<Contacts>
        loading.value = false
    }

    override fun onError(error: Any) {
        responseError.value = error as Repository.ErrorType
        loading.value = false
    }

    override fun onSuccessObjectResponse(response: Object) {
        successSendMoney.value = response as SendMoney
    }


}