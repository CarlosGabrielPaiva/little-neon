package com.paivadeveloper.littleneon.view.home

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.paivadeveloper.littleneon.model.Customer
import com.paivadeveloper.littleneon.service.repository.ServiceListener
import com.paivadeveloper.littleneon.service.repository.Repository

class HomeViewModel(private val repository: Repository): ViewModel(), ServiceListener {

    val tokenLiveData = MutableLiveData<Any>()
    val currentCustomerLiveData = MutableLiveData<Customer>()
    val responseError = MutableLiveData<Repository.ErrorType>()

    fun getToken(){
        repository.getToken(this)
    }

    fun getCurrentCustomerInfo(){
        repository.getCurrentLoggedCustomerInfo(this)
    }

    override fun onSuccess(response: Any) {
        tokenLiveData.value = response
    }

    override fun onError(error: Any) {
        responseError.value = error as Repository.ErrorType
    }
    override fun onSuccessObjectResponse(response: Object) {
        currentCustomerLiveData.value = response as Customer
    }

}