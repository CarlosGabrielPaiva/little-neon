package com.paivadeveloper.littleneon.view.paymenthistory

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.paivadeveloper.littleneon.model.PaymentHistory
import com.paivadeveloper.littleneon.service.repository.Repository
import com.paivadeveloper.littleneon.service.repository.ServiceListener

class PaymentHistoryViewModel(private val repository: Repository): ViewModel(),
   ServiceListener{
    val contactsList = MutableLiveData<List<PaymentHistory>>()
    val responseError = MutableLiveData<Repository.ErrorType>()
    val loading = MutableLiveData<Boolean>()

    override fun onSuccess(response: Any) {
        contactsList.value = response as MutableList<PaymentHistory>
        loading.value = false
    }

    override fun onSuccessObjectResponse(response: Object) {
       //doNothing
    }

    override fun onError(error: Any) {
        responseError.value = error as Repository.ErrorType
        loading.value = false
    }

    fun getPaymentHistory() {
        repository.getPaymentHistory(this)
    }
}