package com.paivadeveloper.littleneon.view.paymenthistory

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.paivadeveloper.littleneon.R
import com.paivadeveloper.littleneon.extensions.getFirstLettersWord
import com.paivadeveloper.littleneon.model.PaymentHistory
import com.squareup.picasso.NetworkPolicy
import com.squareup.picasso.Picasso
import kotlinx.android.synthetic.main.recycler_history_item.view.*
import java.text.NumberFormat

class PaymentHistoryListAdapter(private val paymentList: List<PaymentHistory>): RecyclerView.Adapter<PaymentHistoryListAdapter.ViewHolder>(){
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view = LayoutInflater.from(parent.context)
            .inflate(R.layout.recycler_history_item, parent, false)
        return ViewHolder(view)
    }

    override fun getItemCount(): Int {
        return paymentList.size
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val contact = paymentList[position]

        holder.textViewContactPhoneNumber.text = contact.phoneNumberContact
        holder.textViewHistoryPaymentAmountPayed.text = NumberFormat.getCurrencyInstance().format(contact.payedAmount)
        holder.textViewHistoryPaymentContactName.text = contact.userNameContact

        configProfileImage(holder, contact)

    }

    private fun configProfileImage(
        holder: ViewHolder,
        contact: PaymentHistory) {
        Picasso.get()
            .load(contact.urlProfileImageContact)
            .networkPolicy(NetworkPolicy.OFFLINE)
            .placeholder(R.drawable.ic_placeholder_profile_image)
            .into(holder.imageViewContact)
        if (contact.urlProfileImageContact.equals(NULL_VALUE)) {
            configCardWithNameLetters(contact, holder)
        }
    }

    private fun configCardWithNameLetters(contact: PaymentHistory, holder: ViewHolder) {
        val firstLettersWord = String().getFirstLettersWord(contact.userNameContact!!)
        holder.imageViewContact.visibility = View.INVISIBLE
        holder.constraintLayoutHistoryPaymentFirstLettersWord.visibility = View.VISIBLE
        holder.textViewHistoryPaymentFirstLettersWord.text = firstLettersWord

    }

    class ViewHolder(itemView: View):  RecyclerView.ViewHolder(itemView){
        val textViewHistoryPaymentContactName = itemView.textViewHistoryPaymentContactName
        val textViewContactPhoneNumber = itemView.textViewHistoryPaymentContactPhoneNumber
        val textViewHistoryPaymentAmountPayed = itemView.textViewHistoryPaymentAmountPayed
        val imageViewContact = itemView.circleImageHistoryPaymentContactImage
        val textViewHistoryPaymentFirstLettersWord = itemView.textViewHistoryPaymentFirstLettersWord
        val constraintLayoutHistoryPaymentFirstLettersWord = itemView.constraintLayoutHistoryPaymentFirstLettersWord
    }

    companion object{
        const val NULL_VALUE = "null"
    }


}