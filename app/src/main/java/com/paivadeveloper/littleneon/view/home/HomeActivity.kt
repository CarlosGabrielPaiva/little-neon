package com.paivadeveloper.littleneon.view.home

import android.app.Dialog
import android.content.Intent
import android.os.Bundle
import android.view.Window
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import com.google.android.material.snackbar.Snackbar
import com.paivadeveloper.littleneon.R
import com.paivadeveloper.littleneon.extensions.hasInternetConnection
import com.paivadeveloper.littleneon.service.repository.Repository
import com.paivadeveloper.littleneon.view.paymenthistory.PaymentHistoryActivity
import com.paivadeveloper.littleneon.view.sendmoney.SendMoneyActivity
import com.paivadeveloper.littleneon.viewmodel.ViewModelFactory
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.android.synthetic.main.dialog_error.view.*

class HomeActivity : AppCompatActivity() {

    private val homeViewModel by lazy {
        ViewModelProviders.of(this, ViewModelFactory(this))
            .get(HomeViewModel::class.java)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        initializeCallApis()

        initializeListeners()

        initializeObservers()
    }

    private fun initializeObservers() {
        homeViewModel.currentCustomerLiveData.observe(this, Observer { loggedUser ->
            textViewCurrentCustomerLoggedName.text = loggedUser.loggedCustomerName
            textViewCurrentCustomerLoggedMail.text = loggedUser.loggedCustomerMail
        })
        homeViewModel.responseError.observe(this, Observer {errorType ->
            when(errorType){
                Repository.ErrorType.CONNECTION_ERROR -> showDialogError()
                Repository.ErrorType.CONNECTION_ERROR -> showDialogError()
            }
        })
    }

    private fun showDialogError() {
        val dialog = Dialog(this, R.style.NeonDialog)
        val view = layoutInflater.inflate(R.layout.dialog_error, null)

        view.buttonTryConection.setOnClickListener {
            if (view.context.hasInternetConnection()){
                initializeCallApis()
                dialog.hide()
            }else{
                Snackbar.make(view, R.string.error_connection_try, Snackbar.LENGTH_LONG).show()
            }
        }

        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
        dialog.setCancelable(true)
        dialog.setContentView(view)

        dialog.show()
    }


    private fun initializeListeners() {
        buttonSendMoney.setOnClickListener {
            startActivity(Intent(this, SendMoneyActivity::class.java))
        }
        buttonPaymentHistory.setOnClickListener {
            startActivity(Intent(this, PaymentHistoryActivity::class.java))
        }
    }

    private fun initializeCallApis() {
        homeViewModel.getToken()
        homeViewModel.getCurrentCustomerInfo()
    }
}
