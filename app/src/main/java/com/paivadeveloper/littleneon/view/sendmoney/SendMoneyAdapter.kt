package com.paivadeveloper.littleneon.view.sendmoney

import android.app.Dialog
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.Window
import android.widget.EditText
import androidx.recyclerview.widget.RecyclerView
import com.google.android.material.snackbar.Snackbar
import com.paivadeveloper.littleneon.R
import com.paivadeveloper.littleneon.extensions.getFirstLettersWord
import com.paivadeveloper.littleneon.model.Contacts
import com.paivadeveloper.littleneon.util.MoneyTextWatcher
import com.squareup.picasso.NetworkPolicy
import com.squareup.picasso.Picasso
import kotlinx.android.synthetic.main.dialog_send_money.view.*
import kotlinx.android.synthetic.main.recycler_contacts_item.view.*


class SendMoneyAdapter(
    private var contactsList: List<Contacts>,
    private var sendMoneyActivity: SendMoneyActivity) : RecyclerView.Adapter<SendMoneyAdapter.ViewHolder>() {

    lateinit var editTextSendValue: EditText


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view = LayoutInflater.from(parent.context)
            .inflate(R.layout.recycler_contacts_item, parent, false)
        return ViewHolder(view)
    }
    private fun configRecyclerImageContacts(contact: Contacts, view: View) {

        Picasso.get()
            .load(contact.urlProfileImageContact)
            .networkPolicy(NetworkPolicy.OFFLINE)
            .placeholder(R.drawable.ic_placeholder_profile_image)
            .into(view.circleImageCustomerImageProfile)
    }

    private fun configListeners(view: View, dialog: Dialog, contact: Contacts) {
        view.buttonConfirmSendMoney.setOnClickListener {
            if (!editTextSendValue.text.isNullOrEmpty()){
                sendMoneyActivity.sendPayment(contact, editTextSendValue.text.toString())
                dialog.hide()
            }else{
                Snackbar.make(view, R.string.error_empty_field, Snackbar.LENGTH_LONG).show()
            }
        }
        view.imageViewHideDialog.setOnClickListener {
            dialog.hide()
        }
    }

    override fun getItemCount(): Int {
        return contactsList.size
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val contact = contactsList[position]
        holder.textViewContactName.text = contact.userNameContact
        holder.textViewContactPhoneNumber.text = contact.phoneNumberContact
        configImageProfile(contact, holder)
        holder.itemView.setOnClickListener { showDialog(holder.itemView, contact) }
    }

    private fun configImageProfile(contact: Contacts, holder: ViewHolder) {
        Picasso.get()
            .load(contact.urlProfileImageContact)
            .networkPolicy(NetworkPolicy.OFFLINE)
            .placeholder(R.drawable.ic_placeholder_profile_image)
            .into(holder.imageViewContact)
        if (contact.urlProfileImageContact.equals(NULL_VALUE)) {
           configCardWithNameLetters(contact, holder)
        }
    }

    private fun showDialog(
        view: View,
        contact: Contacts) {

        val dialog = Dialog(view.context, R.style.NeonDialog)
        val view = sendMoneyActivity.layoutInflater.inflate(R.layout.dialog_send_money, null)

        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
        dialog.setCancelable(true)
        dialog.setContentView(view)

        configListeners(view, dialog, contact)
        configRecyclerImageContacts(contact, view)

        editTextSendValue = view.editTextValueSend
        view.textViewCustomerName.text = contact.userNameContact
        view.textViewCustomerPhoneNumber.text = contact.phoneNumberContact
        editTextSendValue.addTextChangedListener(MoneyTextWatcher(editTextSendValue))

        dialog.show()
    }

    private fun configCardWithNameLetters(contact: Contacts, holder: ViewHolder) {
        val firstLettersWord = String().getFirstLettersWord(contact.userNameContact!!)
        holder.imageViewContact.visibility = View.INVISIBLE
        holder.constraintLayoutFirstLettersWord.visibility = View.VISIBLE
        holder.textViewFirstLettersWord.text = firstLettersWord

    }


    class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        val textViewContactName = itemView.textViewContactName
        val textViewContactPhoneNumber = itemView.textViewContactPhoneNumber
        val imageViewContact = itemView.imageViewContact
        val textViewFirstLettersWord = itemView.textViewFirstLettersWord
        val constraintLayoutFirstLettersWord = itemView.constraintLayoutFirstLettersWord
    }


    companion object{
        const val NULL_VALUE = "null"
    }
}