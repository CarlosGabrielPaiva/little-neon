package com.paivadeveloper.littleneon.view.sendmoney

import android.app.Dialog
import android.os.Bundle
import android.view.MenuItem
import android.view.View
import android.view.Window
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.LinearLayoutManager
import com.google.android.material.snackbar.Snackbar
import com.paivadeveloper.littleneon.R
import com.paivadeveloper.littleneon.extensions.hasInternetConnection
import com.paivadeveloper.littleneon.model.Contacts
import com.paivadeveloper.littleneon.service.repository.Repository
import com.paivadeveloper.littleneon.viewmodel.ViewModelFactory
import kotlinx.android.synthetic.main.activity_send_money.*
import kotlinx.android.synthetic.main.content_send_money.*
import kotlinx.android.synthetic.main.dialog_error.view.*


class SendMoneyActivity : AppCompatActivity() {

    private val sendMoneyViewModel by lazy {
        ViewModelProviders.of(this, ViewModelFactory(this))
            .get(SendMoneyViewModel::class.java)
    }


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_send_money)
        setSupportActionBar(toolbar)

        configRecyclerView()
        setObservers()
        configToolbar()

        sendMoneyViewModel.getContacts()
    }

    private fun setObservers() {
        sendMoneyViewModel.successSendMoney.observe(this, Observer {
            val parentLayout = findViewById<View>(android.R.id.content)
            Snackbar.make(parentLayout, R.string.success_send_value, Snackbar.LENGTH_LONG).show()
        })
        sendMoneyViewModel.contactsList.observe(this, Observer {contacts ->
            recyclerListContactsSendMoney.adapter = SendMoneyAdapter(contacts, this@SendMoneyActivity)
        })
        sendMoneyViewModel.loading.observe(this, Observer { isVisibleLoading ->
            when (isVisibleLoading) {
                true -> showLoading()
                false -> hideLoading()
            }
        })

        sendMoneyViewModel.responseError.observe(this, Observer { errorType ->
            when (errorType) {
                Repository.ErrorType.API_ERROR ->
                    showError(ERROR_API)

                Repository.ErrorType.CONNECTION_ERROR ->
                    showError(ERROR_INTERNET)
            }
        })
    }

    private fun showError(errorMessage: String) {
        val dialog = Dialog(this, R.style.NeonDialog)
        val view = layoutInflater.inflate(R.layout.dialog_error, null)


        view.textViewDialogErrorSubtitle.text = errorMessage
        view.buttonTryConection.text = ERROR_OK
        view.buttonTryConection.setOnClickListener {
            if (view.context.hasInternetConnection()){
                dialog.hide()
            }else{
                Snackbar.make(view, R.string.error_connection, Snackbar.LENGTH_LONG).show()
            }

        }

        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
        dialog.setCancelable(true)
        dialog.setContentView(view)

        dialog.show()

    }

    private fun hideLoading() {
        lottieError.visibility = View.GONE
        recyclerListContactsSendMoney.visibility = View.VISIBLE
    }

    private fun showLoading() {
        lottieError.visibility = View.VISIBLE
        recyclerListContactsSendMoney.visibility = View.GONE
    }

    private fun configToolbar() {
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        supportActionBar?.setDisplayShowHomeEnabled(true)
        val backArrow = resources.getDrawable(R.drawable.ic_left_arrow)
        supportActionBar?.setHomeAsUpIndicator(backArrow)
    }

    private fun configRecyclerView() {
        recyclerListContactsSendMoney.adapter = SendMoneyAdapter(
            emptyList(),
            this
        )
        val layoutManager = LinearLayoutManager(this)
        recyclerListContactsSendMoney.layoutManager = layoutManager
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        if (item.itemId === android.R.id.home) {
            finish()
        }

        return super.onOptionsItemSelected(item)
    }

    fun sendPayment(
        contact: Contacts,
        amount: String
    ) {
        sendMoneyViewModel.sendMoney(contact, amount)
    }

    companion object{
        const val ERROR_INTERNET = "Nossos servidores estão em manutenção ;("
        const val ERROR_API = "Nossos servidores estão em manutenção ;("
        const val ERROR_OK = "ok ;("
    }
}
