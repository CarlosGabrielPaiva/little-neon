package com.paivadeveloper.littleneon.service.retrofit

import com.paivadeveloper.littleneon.BuildConfig
import retrofit2.Retrofit
import retrofit2.converter.jackson.JacksonConverterFactory


object RetrofitService {

    private val retrofit: Retrofit = Retrofit.Builder()
        .baseUrl(BuildConfig.API)
        .addConverterFactory(JacksonConverterFactory.create())
        .build()

    fun getService(): Api = retrofit.create(
        Api::class.java)
}