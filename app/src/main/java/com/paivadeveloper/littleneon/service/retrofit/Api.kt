package com.paivadeveloper.littleneon.service.retrofit

import com.paivadeveloper.littleneon.model.Contacts
import com.paivadeveloper.littleneon.model.Customer
import com.paivadeveloper.littleneon.model.PaymentHistory
import com.paivadeveloper.littleneon.model.SendMoney
import retrofit2.Call
import retrofit2.http.GET
import retrofit2.http.POST

interface Api {

    @GET("/getContactList")
    fun getContacts(): Call<MutableList<Contacts>>

    @POST("/generateToken")
    fun getToken(): Call<String>

    @GET("/currentUser")
    fun getCurrentCustomerLoggedInfo(): Call<Customer>

    @POST("/sendMoney")
    fun sendMoney(): Call<SendMoney>

    @GET("/getPaymentHistory")
    fun getPaymentHistory(): Call<MutableList<PaymentHistory>>
}