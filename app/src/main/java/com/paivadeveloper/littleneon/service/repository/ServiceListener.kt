package com.paivadeveloper.littleneon.service.repository

interface ServiceListener {

    fun onSuccess(response:Any)
    fun onSuccessObjectResponse(response:Object)
    fun onError(error: Any)
}