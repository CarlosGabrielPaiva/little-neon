package com.paivadeveloper.littleneon.service.repository

import android.content.Context
import com.paivadeveloper.littleneon.extensions.hasInternetConnection
import com.paivadeveloper.littleneon.model.*
import com.paivadeveloper.littleneon.service.retrofit.RetrofitService
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class Repository(private val context: Context) {
    enum class ErrorType {
        CONNECTION_ERROR,
        API_ERROR
    }


    fun getContacts(listener: ServiceListener) {
        if (context.hasInternetConnection()) {
            val call = RetrofitService.getService().getContacts()
            call.enqueue(object : Callback<MutableList<Contacts>> {
                override fun onFailure(call: Call<MutableList<Contacts>>, t: Throwable) {
                    listener.onError(ErrorType.API_ERROR)
                }

                override fun onResponse(
                    call: Call<MutableList<Contacts>>,
                    response: Response<MutableList<Contacts>>
                ) {
                    response.body()?.let { response ->
                        listener.onSuccess(response)
                    }
                }
            })
        } else {
            listener.onError(ErrorType.CONNECTION_ERROR)
        }

    }

    fun getToken(listener: ServiceListener) {
        val call = RetrofitService.getService().getToken()

        if (context.hasInternetConnection()) {
            call.enqueue(object : Callback<String> {
                override fun onFailure(call: Call<String>, t: Throwable) {
                    listener.onError(ErrorType.API_ERROR)
                }

                override fun onResponse(
                    call: Call<String>,
                    response: Response<String>
                ) {
                    response.body()?.let {
                        listener.onSuccess(it)
                        UserInstance.loggedCustomerToken = response.body()
                    }
                }
            })
        } else {
            listener.onError(ErrorType.CONNECTION_ERROR)
        }
    }


    fun getCurrentLoggedCustomerInfo(listener: ServiceListener) {
        val call = RetrofitService.getService().getCurrentCustomerLoggedInfo()

        if (context.hasInternetConnection()) {
            call.enqueue(object : Callback<Customer> {
                override fun onFailure(call: Call<Customer>, t: Throwable) {
                    listener.onError(ErrorType.API_ERROR)
                }

                override fun onResponse(call: Call<Customer>, response: Response<Customer>) {
                    listener.onSuccessObjectResponse(response.body() as Object)
                }
            })
        } else {
            listener.onError(ErrorType.CONNECTION_ERROR)
        }
    }

    fun sendMoney(
        listener: ServiceListener,
        contact: Contacts,
        amount: String) {

        if (context.hasInternetConnection()) {
            val call = RetrofitService.getService().sendMoney()
            call.enqueue(object : Callback<SendMoney> {
                override fun onFailure(call: Call<SendMoney>, t: Throwable) {
                    listener.onError(ErrorType.API_ERROR)
                }

                override fun onResponse(call: Call<SendMoney>, response: Response<SendMoney>) {
                    listener.onSuccessObjectResponse(response.body() as Object)
                }
            })
        } else {
            listener.onError(ErrorType.CONNECTION_ERROR)
        }
    }

    fun getPaymentHistory(listener: ServiceListener){

        if (context.hasInternetConnection()) {
            val call = RetrofitService.getService().getPaymentHistory()
           call.enqueue(object : Callback<MutableList<PaymentHistory>>{
               override fun onFailure(call: Call<MutableList<PaymentHistory>>, t: Throwable) {

               }

               override fun onResponse(
                   call: Call<MutableList<PaymentHistory>>,
                   response: Response<MutableList<PaymentHistory>>
               ) {
                   response.body()?.let { response ->
                       listener.onSuccess(response)
                   }
               }

           })
        } else {
            listener.onError(ErrorType.CONNECTION_ERROR)
        }
    }
}