package com.paivadeveloper.littleneon.extensions



fun String.getFirstLettersWord(string: String): String{
    var result = ""
    var v = true
    for (i in string.indices) {
        if (string[i] == ' ') {
            v = true
        } else if (string[i] != ' ' && v) {
            result += string[i]
            v = false
        }
    }
    return result
}