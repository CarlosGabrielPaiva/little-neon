package com.paivadeveloper.littleneon.model

import java.io.Serializable

data class SendMoney(var successOperation: Boolean = false): Serializable