package com.paivadeveloper.littleneon.model

import java.io.Serializable

data class PaymentHistory( var phoneNumberContact: String? = null, var urlProfileImageContact: String? = null,
                     var userNameContact: String? = null,  var id: String? = null,
                           var payedAmount: Double? = 0.0) : Serializable