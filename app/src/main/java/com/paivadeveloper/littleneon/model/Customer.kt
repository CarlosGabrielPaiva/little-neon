package com.paivadeveloper.littleneon.model

import java.io.Serializable

data class Customer( var loggedCustomerName: String? = null,
                     var loggedCustomerMail: String? = null,
                     var urlImageProfile: String? = null): Serializable
