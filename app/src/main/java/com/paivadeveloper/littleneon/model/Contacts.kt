package com.paivadeveloper.littleneon.model

import java.io.Serializable

data class Contacts( var phoneNumberContact: String? = null, var urlProfileImageContact: String? = null,
                     var userNameContact: String? = null,  var id: String? = null) : Serializable
