package com.paivadeveloper.littleneon.util

import android.text.Editable
import android.text.TextWatcher
import android.widget.EditText
import java.text.NumberFormat
import java.util.*


class MoneyTextWatcher(private val editText: EditText) : TextWatcher {

    private var lastAmount = ""

    private var lastCursorPosition = -1

    override fun onTextChanged(amount: CharSequence, start: Int, before: Int, count: Int) {

        if (amount.toString() != lastAmount) {

            val cleanString = clearCurrencyToNumber(amount.toString())

            try {

                val formattedAmount = transformToCurrency(cleanString)
                editText.removeTextChangedListener(this)
                editText.setText(formattedAmount)
                editText.setSelection(formattedAmount.length)
                editText.addTextChangedListener(this)

                if (lastCursorPosition != lastAmount.length && lastCursorPosition != -1) {
                    val lengthDelta = formattedAmount.length - lastAmount.length

                    editText.setSelection(lengthDelta)
                }
            } catch (e: Exception) {
            //doNothing
            }

        }
    }

    override fun afterTextChanged(s: Editable) {}

    override fun beforeTextChanged(s: CharSequence, start: Int, count: Int, after: Int) {
        val value = s.toString()
        if (value != "") {
            val cleanString = clearCurrencyToNumber(value)
            val formattedAmount = transformToCurrency(cleanString)
            lastAmount = formattedAmount
            lastCursorPosition = editText.selectionStart
        }
    }

    companion object {

        fun clearCurrencyToNumber(currencyValue: String?): String {
            var result: String?
            result = currencyValue?.replace("[(a-z)|(A-Z)|($,. )]".toRegex(), "") ?: ""
            return result
        }

        fun transformToCurrency(value: String): String {
            val parsed = java.lang.Double.parseDouble(value)
            var formatted =
                NumberFormat.getCurrencyInstance(Locale("pt", "BR")).format(parsed / 100)
            formatted = formatted.replace("[^(0-9)(.,)]".toRegex(), "")
            return formatted
        }
    }
}
