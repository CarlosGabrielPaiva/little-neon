package com.paivadeveloper.littleneon.viewmodel

import android.content.Context
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.paivadeveloper.littleneon.service.repository.Repository
import com.paivadeveloper.littleneon.view.home.HomeViewModel
import com.paivadeveloper.littleneon.view.paymenthistory.PaymentHistoryViewModel
import com.paivadeveloper.littleneon.view.sendmoney.SendMoneyViewModel

class ViewModelFactory(context:Context) : ViewModelProvider.NewInstanceFactory() {

    private val repository = Repository(context)

    override fun <T : ViewModel?> create(modelClass: Class<T>): T =
        with(modelClass) {

            when {
                isAssignableFrom(SendMoneyViewModel::class.java) ->
                    SendMoneyViewModel(repository)

                isAssignableFrom(HomeViewModel::class.java)->
                    HomeViewModel(repository)

                isAssignableFrom(PaymentHistoryViewModel::class.java) ->
                    PaymentHistoryViewModel(repository)

                else ->
                    throw IllegalArgumentException("Unknown ViewModel class: ${modelClass.name}")
            }

        } as T
}